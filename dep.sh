#!/bin/bash

# Update sistem dan lewati permintaan interaktif
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y -q

# Instal Python 3 dan pip3
sudo apt install -y python3 python3-pip

# Instal unzip
sudo apt install -y unzip

# Instal paket Python menggunakan pip3
pip3 install requests oci pyTelegramBotAPI

# Informasi setelah instalasi selesai
echo "Instalasi selesai. Python 3, pip3, unzip, dan paket Python telah diinstal."

# Tampilkan versi Python
python3 --version

# Tampilkan versi pip
pip3 --version
