import requests
from zipfile import ZipFile
import os

def download_and_unzip(base_url, username):
    file_name = f"{username}.zip"
    zip_url = f"{base_url}/{file_name}"

    response = requests.get(zip_url)
    
    if response.status_code == 200:
        with open(file_name, 'wb') as f:
            f.write(response.content)

        with ZipFile(file_name, 'r') as zip_ref:
            zip_ref.extractall()

        os.remove(file_name)

        print(f"File {file_name} berhasil diunduh dan diekstrak.")
    else:
        print(f"Gagal mendownload file. Status code: {response.status_code}")

if __name__ == "__main__":
    base_url = "https://gitlab.com/Zeusyf/Akebenayasomsomheuhe/-/raw/main"
    
    username = input("Masukkan nama pengguna akun opc (misalnya, nomauiias120): ")

    download_and_unzip(base_url, username)

    os.system("python3 botixi.py")
